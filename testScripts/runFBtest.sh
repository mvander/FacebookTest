#!/bin/sh

#  runFBtest.sh
#  
#
#  Created by Mark Van der Merwe on 7/26/16.
#

if [ $# -lt 2 ]; then
echo "Usage: <UE #1 port #, eg, 8001> <UE #2 port #, eg, 8001>"
exit 1
fi

UE1=$1
UE2=$2

./FBPhone1A.py -s pc599.emulab.net:$UE2
./FBPhone2.py -s pc599.emulab.net:$UE1
./FBPhone1B.py -s pc599.emulab.net:$UE2