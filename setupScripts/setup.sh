#!/bin/sh

#  setupOnTgt.sh
#  
#
#  Created by Mark Van der Merwe on 7/21/16.
#

if [ $# -lt 2 ]; then
echo "Usage: <UE #1 port #, eg, 8001> <UE #2 port #, eg, 8001>"
exit 1
fi

UE1=$1
UE2=$2

adb -s pc599.emulab.net:$UE1 install aro.apk
adb -s pc599.emulab.net:$UE1 install kingoroot.apk
adb -s pc599.emulab.net:$UE1 install facebook.apk

adb -s pc599.emulab.net:$UE2 install aro.apk
adb -s pc599.emulab.net:$UE2 install kingoroot.apk
adb -s pc599.emulab.net:$UE2 install facebook.apk

adb -s pc599.emulab.net:$UE1 push SLCPic.jpg /sdcard/SLC.jpg

./rootStart.py -s pc599.emulab.net:$UE1
sleep 3m
./rootStop.py -s pc599.emulab.net:$UE1

./rootStart.py -s pc599.emulab.net:$UE2
sleep 3m
./rootStopTwo.py -s pc599.emulab.net:$UE2

echo "Set up, ready for test."