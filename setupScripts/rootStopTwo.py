#! /usr/bin/env python
# -*- coding: utf-8 -*-
'''
Copyright (C) 2013-2016  Diego Torres Milano
Created on 2016-07-22 by Culebra v11.5.8
                      __    __    __    __
                     /  \  /  \  /  \  /  \ 
____________________/  __\/  __\/  __\/  __\_____________________________
___________________/  /__/  /__/  /__/  /________________________________
                   | / \   / \   / \   / \   \___
                   |/   \_/   \_/   \_/   \    o \ 
                                           \_____/--<
@author: Diego Torres Milano
@author: Jennifer E. Swofford (ascii art snake)
'''


import re
import sys
import os


import unittest

from com.dtmilano.android.viewclient import ViewClient, CulebraTestCase

TAG = 'CULEBRA'


class CulebraTests(CulebraTestCase):

    @classmethod
    def setUpClass(cls):
        cls.kwargs1 = {'ignoreversioncheck': False, 'verbose': False, 'ignoresecuredevice': False}
        cls.kwargs2 = {'forceviewserveruse': False, 'useuiautomatorhelper': False, 'ignoreuiautomatorkilled': True, 'autodump': False, 'startviewserver': True, 'compresseddump': True}
        cls.options = {'start-activity': None, 'concertina': False, 'device-art': None, 'use-jar': False, 'multi-device': False, 'unit-test-class': True, 'save-screenshot': None, 'use-dictionary': False, 'glare': False, 'dictionary-keys-from': 'id', 'scale': 0.25, 'find-views-with-content-description': True, 'window': -1, 'orientation-locked': None, 'save-view-screenshots': None, 'find-views-by-id': True, 'log-actions': False, 'use-regexps': False, 'null-back-end': False, 'auto-regexps': None, 'do-not-verify-screen-dump': True, 'verbose-comments': False, 'gui': True, 'find-views-with-text': True, 'prepend-to-sys-path': False, 'install-apk': None, 'drop-shadow': False, 'output': 'blah.py', 'unit-test-method': None, 'interactive': False}
        cls.sleep = 5

    def setUp(self):
        super(CulebraTests, self).setUp()

    def tearDown(self):
        super(CulebraTests, self).tearDown()

    def preconditions(self):
        if not super(CulebraTests, self).preconditions():
            return False
        return True

    def testSomething(self):
        if not self.preconditions():
            self.fail('Preconditions failed')

        _s = CulebraTests.sleep
        _v = CulebraTests.verbose

        self.vc.dump(window=-1)
        self.device.touchDip(288.0, 612.0, 0)
        self.vc.sleep(_s)
        self.vc.dump(window=-1)
        self.vc.sleep(_s)
        self.vc.dump(window=-1)
        self.device.dragDip((281.33, 530.67), (12.0, 524.0), 1000, 20, 0)
        self.vc.sleep(1)
        self.vc.dump(window=-1)
        self.device.touchDip(181.33, 621.33, 0)
        self.vc.sleep(_s)
        self.vc.dump(window=-1)
        self.device.touchDip(177.33, 617.33, 0)
        self.vc.sleep(_s)
        self.vc.dump(window=-1)
        self.vc.findViewWithContentDescriptionOrRaise(u'''Apps''').touch()
        self.vc.sleep(_s)
        self.vc.dump(window=-1)
        self.vc.findViewWithTextOrRaise(u'ARO').touch()
        self.vc.sleep(_s)
        self.vc.dump(window=-1)
        self.vc.findViewWithTextOrRaise(u'ALLOW').touch()
        self.vc.sleep(_s)
        self.vc.dump(window=-1)
        self.vc.findViewWithTextOrRaise(u'Accept').touch()
        self.vc.sleep(_s)
        self.vc.dump(window=-1)
        self.device.touchDip(181.33, 621.33, 0)
        self.vc.sleep(_s)
        self.vc.dump(window=-1)
        self.vc.sleep(_s)
        self.vc.dump(window=-1)
        self.device.touchDip(180.0, 548.0, 0)
        self.vc.sleep(_s)
        self.vc.dump(window=-1)
        self.vc.findViewWithTextOrRaise(u'Settings').touch()
        self.vc.sleep(_s)
        self.vc.dump(window=-1)
        self.device.dragDip((162.67, 568.0), (160.0, 44.0), 1000, 20, 0)
        self.vc.sleep(1)
        self.vc.dump(window=-1)
        self.vc.findViewWithTextOrRaise(u'Date & time', root=self.vc.findViewByIdOrRaise('id/no_id/18')).touch()
        self.vc.sleep(_s)
        self.vc.dump(window=-1)
        self.vc.findViewWithTextOrRaise(u'Use network-provided time', root=self.vc.findViewByIdOrRaise('id/no_id/6')).touch()
        self.vc.sleep(_s)
        self.vc.dump(window=-1)
        self.device.touchDip(168.0, 230.67, 0)
        self.vc.sleep(_s)
        self.vc.dump(window=-1)
        self.device.touchDip(261.33, 321.33, 0)
        self.vc.sleep(_s)
        self.vc.dump(window=-1)
        self.device.touchDip(150.67, 396.0, 0)
        self.vc.sleep(_s)
        self.vc.dump(window=-1)
        self.vc.sleep(_s)
        self.vc.dump(window=-1)
        self.device.touchDip(144.0, 552.0, 0)
        self.vc.sleep(_s)
        self.vc.dump(window=-1)
        self.device.touchDip(69.33, 401.33, 0)
        self.vc.sleep(_s)
        self.vc.dump(window=-1)
        self.vc.sleep(_s)
        self.vc.dump(window=-1)
        self.device.touchDip(221.33, 445.33, 0)
        self.vc.sleep(_s)
        self.vc.dump(window=-1)
        self.vc.findViewWithTextOrRaise(u'Done').touch()
        self.vc.sleep(_s)
        self.vc.dump(window=-1)
        self.device.touchDip(288.0, 617.33, 0)
        self.vc.sleep(_s)
        self.vc.dump(window=-1)
        self.vc.sleep(_s)
        self.vc.dump(window=-1)
        self.device.dragDip((300.0, 518.67), (8.0, 522.67), 1000, 20, 0)
        self.vc.sleep(1)
        self.vc.dump(window=-1)
        self.device.touchDip(181.33, 621.33, 0)
        self.vc.sleep(_s)
        self.vc.dump(window=-1)
        self.vc.findViewWithContentDescriptionOrRaise(u'''Apps''').touch()
        self.vc.sleep(_s)
        self.vc.dump(window=-1)
        self.vc.findViewWithTextOrRaise(u'Facebook').touch()
        self.vc.sleep(_s)
        self.vc.dump(window=-1)
        self.vc.findViewWithTextOrRaise(u'Email or Phone').setText(u"markfluxutahtwo@gmail.com")
        self.vc.sleep(_s)
        self.vc.dump(window=-1)
        self.vc.findViewByIdOrRaise("com.facebook.katana:id/login_password").setText(u"m@rkflux")
        self.vc.sleep(_s)
        self.vc.dump(window=-1)
        self.vc.findViewWithTextOrRaise(u'LOG IN').touch()
        self.vc.sleep(_s)
        self.vc.dump(window=-1)
        self.device.touchDip(276.0, 622.67, 0)
        self.vc.sleep(_s)
        self.vc.dump(window=-1)
        self.vc.sleep(_s)
        self.vc.dump(window=-1)
        self.device.dragDip((304.0, 517.33), (2.67, 505.33), 1000, 20, 0)
        self.vc.sleep(1)
        self.vc.dump(window=-1)
        self.device.touchDip(180.0, 624.0, 0)
        self.vc.sleep(_s)
        self.vc.dump(window=-1)



if __name__ == '__main__':
    CulebraTests.main()

